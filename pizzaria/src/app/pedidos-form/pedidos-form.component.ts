import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-pedidos-form',
	templateUrl: './pedidos-form.component.html',
	styleUrls: ['./pedidos-form.component.css']
})
export class PedidosFormComponent implements OnInit {

	public pedido: any = {};
	public pizzas: Array<any> = [];
	public bebidas: Array<any> = [];
	public listaPedidos: Array<any> = [];
	public valorTotal = 0;

	constructor(
		private apiService: ApiService,
		private router: Router
	) { }

	ngOnInit() {
		this.apiService.getPizza().subscribe((data: any) => {
			this.pizzas = data;
		}, error => {
			console.log(error);
		});

		this.apiService.getBebidas().subscribe((data: any) => {
			this.bebidas = data;
		}, error => {
			console.log(error);
		});
	}

	public calculaTotal () {
		return parseFloat(this.pedido.bebida["Preço"]) + parseFloat(this.pedido.pizza["Preço"]);
	}

	public adicionar() {
		let total = this.calculaTotal();
		this.listaPedidos.push({
			bebida: this.pedido.bebida,
			pizza: this.pedido.pizza,
			total: total,
		});
		this.pedido = {};
		this.valorTotal += total;
		console.log(this.listaPedidos);
	}

	public salvar() {
		this.apiService.salvarPedido(this.listaPedidos).subscribe(data => {
			console.log("Pedido inserido!");
		})
	}
}
