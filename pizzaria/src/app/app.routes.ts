import { Routes, CanActivate, RouterModule } from '@angular/router';
import { FormComponent } from '../app//form/form.component';
import { ListaComponent } from './lista/lista.component';
import { BebidasFormComponent } from './bebidas-form/bebidas-form.component';
import { BebidasListComponent } from './bebidas-list/bebidas-list.component';
import { PedidosFormComponent } from './pedidos-form/pedidos-form.component';
import { PedidosListaComponent } from './pedidos-lista/pedidos-lista.component';

export const appRoutes : Routes = [
    { path: 'nova-pizza', component: FormComponent },
    { path: 'lista-pizzas', component: ListaComponent },
    { path: 'nova-bebida', component: BebidasFormComponent },
    { path: 'lista-bebidas', component: BebidasListComponent },
    { path: 'novo-pedido', component: PedidosFormComponent },
    { path: 'lista-pedidos', component: PedidosListaComponent }
];

export const AppRoutes = RouterModule.forRoot(appRoutes, { useHash: true });
