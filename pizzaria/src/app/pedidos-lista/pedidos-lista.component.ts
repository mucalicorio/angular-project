import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pedidos-lista',
  templateUrl: './pedidos-lista.component.html',
  styleUrls: ['./pedidos-lista.component.css']
})
export class PedidosListaComponent implements OnInit {

	public lista: Array<any> = [];

  constructor(
  	private apiService: ApiService,
  	private router: Router
  ) { }

  ngOnInit() {
  	this.apiService.getPedido().subscribe((data : Array<any>) => {
  		this.lista = data;
      for (let pedido of this.lista) {
        pedido.valorTotal = 0;
        for (let p of pedido) {
          pedido.valorTotal += p.total;
        }
      }

  		console.log(this.lista);
  	});
  }

  remover(pedidoId) {
  	this.apiService.removerPedidos(pedidoId).subscribe(data => {
  		console.log("removeu");
  		location.reload();
  	}, error => {
  		console.log(error);
  	})
  }
}
